<?php
    defined('_JEXEC') or die;
    $document = JFactory::getDocument();
    $app  = JFactory::getApplication();
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<jdoc:include type="head" />
    <!-- Bootstrap Core CSS -->
	<?php 
		$document->addStyleSheet('templates/' . $this->template .'/css/bootstrap.min.css');
		$document->addStyleSheet('templates/' . $this->template .'/css/font-awesome/css/font-awesome.min.css');
		$document->addStyleSheet('templates/' . $this->template .'/css/style.css');
	?>
</head>
<body>
	<jdoc:include type="message" />
	<jdoc:include type="component" />
</body>
</html>